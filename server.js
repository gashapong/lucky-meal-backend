const express = require("express")
const { port } = require("./src/config")
const router = require("./src/router")
const { enableCors } = require("./src/router/middleware")

const app = express()

app.use(enableCors())

app.use(router)

app.listen(port, () => {
    var today = new Date()
    console.log("lucky meal web server running at port", port, today.toDateString(), today.toLocaleTimeString())
})
