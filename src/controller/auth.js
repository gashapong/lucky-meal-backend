exports = module.exports = {}

exports.logout = (req, res) => {
    req.session.destroy()
    res.json("ออกจากระบบสำเร็จ")
}