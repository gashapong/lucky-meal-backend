const couponModel = require("../models/coupon")

exports = module.exports = {}

exports.customerCouponInfo = async (req, res) => {
    const isAvailable = await couponModel.isCouponAvailable(req.query.coupon_id, req.session.user_id)
    if (isAvailable) {
        const result = await couponModel.getCustomerCoupon(req.query.coupon_id)
        res.send(result)
    }
    else {
        throw "ไม่มีสิทธิ์ดูข้อมูลของคูปองนี้ได้"
    }
}

exports.useCoupon = async (req, res) => {
    const isAvailable = await couponModel.isCouponAvailable(req.query.coupon_id, req.session.user_id)
    if (isAvailable) {
        const result = await couponModel.deleteCoupon(req.query.coupon_id)
        res.send(result)
    }
    else {
        throw "ไม่สามารถใช้คูปองนี้ได้"
    }
}