const adminModel = require("../models/admin.js")
const couponReqModel = require("../models/coupon_req")
const gashaponModel = require("../models/gashapon")
const validator = require("../models/validator")

exports = module.exports = {}

exports.loginAdmin = async (req, res) => {
    validator.checkUserValidation(req.body.username, req.body.password)
    const result = await adminModel.getAdminByUsername(req.body.username)
    if (result && (req.body.password === result.password)) {
        req.session.authenticated = true
        req.session.user_id = result.user_id
        req.session.username = result.username
        req.session.user_type = "Admin"
        res.json("เข้าสู่ระบบสำเร็จ")
    }
    else {
        throw "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง"
    }
}

exports.getAllCouponReq = async (req, res) => {
    const result = await couponReqModel.getAllCouponReq()
    res.json(result)
}

exports.getCouponReq = async (req, res) => {
    const couponReq = await couponReqModel.getCouponReqByReqId(req.query.req_id)
    const shopInfo = await couponReqModel.getShopByReqId(req.query.req_id)
    const location = await couponReqModel.getCouponReqLocation(req.query.req_id)
    const rarity = await couponReqModel.getCouponsRateByReqId(req.query.req_id)
    res.json({ couponReq, shopInfo, location, raity: rarity })
}

exports.acceptCouponReq = async (req, res) => {
    const couponReq = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    if (couponReq.status !== "waiting") {
        throw "ไม่สามารถอนุมัติคูปองนี้ได้"
    }
    const includes = await findGasha(req.body, couponReq)
    const result = await couponReqModel.acceptCouponReq(includes.rate_include, includes.gasha_include, req.body.req_id)
    res.json(result)
}

exports.editCouponReq = async (req, res) => {
    const couponReq = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    if (!["confirm", "end"].includes(couponReq.status)) {
        throw "ไม่สามารถแก้ไขคูปองนี้ได้"
    }
    const includes = await findGasha(req.body, couponReq)
    const result = await couponReqModel.editCouponReqRate(includes.rate_include, includes.gasha_include, req.body.req_id)
    res.json(result)
}

exports.rejectCouponReq = async (req, res) => {
    const couponReq = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    if (couponReq.status !== "waiting") {
        throw "ไม่สามารถปฏิเสธคูปองนี้ได้"
    }
    const status = "reject"
    const result = await couponReqModel.changeStatusCouponReq(status, req.body.req_id)
    res.json(result)
}

const findGasha = async (object, couponReq) => {
    const location = await couponReqModel.getCouponReqLocation(object.req_id)
    let rate_include = {}
    let gasha_include = {}
    for (let key in object) {
        let price
        if (key === "cookies10" && object[key]) price = 10
        else if (key === "cookies19" && object[key]) price = 19
        else if (key === "cookies28" && object[key]) price = 28
        else if (key === "cookies37" && object[key]) price = 37
        else continue
        gasha_include[key] = []
        rate_include[key] = object[key]
        // rate_include[key] = JSON.parse(object[key])
        for (let loc of location) {
            const gasha = await gashaponModel.searchGashapon(loc, couponReq.food_type, price)
            if (gasha[0]) {
                gasha_include[key].push(gasha[0].gasha_id)
            }
        }
    }
    return { gasha_include, rate_include }
}

exports.autoClear = async (req, res) => {
    const result = await couponReqModel.autoClear()
    res.json(result)
}