const userModel = require("../models/user")
const transactionModel = require("../models/transaction")
const couponModel = require("../models/coupon")
const couponReqModel = require("../models/coupon_req")
const validator = require("../models/validator")

exports = module.exports = {}

exports.registerCustomer = async (req, res) => {
    validator.checkCustomerValidation(req.body.username, req.body.password, req.body.fname, req.body.lname, req.body.email, req.body.telephone)
    const EmailAvailable = await userModel.isEmailAvailable(req.body.email)
    const UserAvailable = await userModel.isUsernameAvailable(req.body.username)
    if (!EmailAvailable) {
        throw "อีเมลนี้มีผู้ใช้งานแล้ว"
    }
    else if (!UserAvailable) {
        throw "ชื่อผู้ใช้นี้มีผู้ใช้งานแล้ว"
    }
    const result = await userModel.createCustomer(req.body.username, req.body.password, req.body.fname, req.body.lname, req.body.email, req.body.telephone)
    res.json(result)
}

exports.loginCustomer = async (req, res) => {
    validator.checkUserValidation(req.body.username, req.body.password)
    const result = await userModel.getCustomerByUsername(req.body.username)
    if (result && (req.body.password === result.password)) {
        req.session.authenticated = true
        req.session.user_id = result.user_id
        req.session.username = result.username
        req.session.user_type = "Customer"
        res.json("เข้าสู่ระบบสำเร็จ")
    }
    else {
        throw "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง"
    }
}

exports.customerInfo = async (req, res) => {
    const resultInfo = await userModel.getCustomerById(req.session.user_id)
    const resultValues = await transactionModel.sumValuesByUserId(req.session.user_id)
    res.json(Object.assign({}, resultInfo, resultValues))
}

exports.editCustomerInfo = async (req, res) => {
    validator.checkEditCustomerValidation(req.body.fname, req.body.lname, req.body.telephone)
    const result = await userModel.editCustomerInfo(req.body.fname, req.body.lname, req.body.telephone, req.session.user_id)
    res.json(result)
}

exports.getCoupon = async (req, res) => {
    const coupon = await couponModel.getCouponsByCusId(req.session.user_id)
    const couponWithLoc = await Promise.all(coupon.map(async (result) => {
        const location = await couponReqModel.getCouponReqLocation(result.req_id)
        return Object.assign({}, result, { location })
    }))
    res.json(couponWithLoc)
}
