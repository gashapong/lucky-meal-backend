const transactionModel = require("../models/transaction")
const validator = require("../models/validator")
const userModel = require("../models/user")

exports = module.exports = {}

exports.topUp = async (req, res) => {
    if(req.body.values < 0) {
        throw "จำนวนเงินที่ต้องการเพิ่มในระบบไม่ถูกต้อง"
    }
    validator.checkTopUpValidation(req.body.card_number, req.body.expiration, req.body.cvc)
    const result = await transactionModel.transactionSystem(req.session.user_id, "top up", req.body.values)
    res.json(result)
}

exports.getCredits = async (req, res) => {
    const result = await transactionModel.sumValuesByUserId(req.session.user_id)
    res.json(result)
}

exports.getShopPaymentInfo = async (req, res) => {
    const result = await userModel.getAllShop()
    const resultWithValues = await Promise.all(result.map(async (result) => {
        const values = await transactionModel.sumValuesByUserId(result.shop_id)
        return Object.assign({}, result, values)
    }))
    const resultWithHasValues = await resultWithValues.filter((result) => {
        return result.values > 0
    })
    res.json(resultWithHasValues)
}

exports.paidToShop = async (req, res) => {
    const currentCredits = await transactionModel.sumValuesByUserId(req.body.user_id)
    if (req.body.values > currentCredits) {
        throw "ไม่สามารถโอนเงินจำนวนนี้ได้"
    }
    const result = await transactionModel.transactionSystem(req.body.user_id, "compensation", -req.body.values)
    res.json(result)
}