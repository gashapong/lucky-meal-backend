const gashaponModel = require("../models/gashapon")
const couponReqModel = require("../models/coupon_req")
const couponModel = require("../models/coupon")
const transactionModel = require("../models/transaction")

exports = module.exports = {}

exports.filterGashapon = (req, res) => {
    const result = gashaponModel.getGashaponFilter()
    res.json(result)
}

exports.getAllLocation = (req, res) => {
    const result = gashaponModel.getAllLocation()
    res.json(result)
}

exports.getAllFoodType = (req, res) => {
    const result = gashaponModel.getAllFoodType()
    res.json(result)
}

exports.searchGashapon = async (req, res) => {
    const result = await gashaponModel.searchAviableGashapon(req.query.location, req.query.type, req.query.price)
    res.json(result)
}

exports.couponsInGashapon = async (req, res) => {
    const result = await couponReqModel.getCouponsInfoByGashaId(req.query.gasha_id)
    res.json(result)
}

exports.getCouponReqRate = async (req, res) => {
    const result = await couponReqModel.getCouponsRateByGashaId(req.query.gasha_id)
    res.json(result)
}

exports.canDraw = async (req, res) => {
    const gashaponInfo = await gashaponModel.getGashaponByGashaId(req.query.gasha_id)
    const userValue = await transactionModel.sumValuesByUserId(req.session.user_id)
    if (userValue.values < gashaponInfo[0].price) {
        throw "cookies ไม่พอสำหรับการสุ่มกาชาปอง"
    }
    res.json("สามารถสุ่มกาชาปองได้")
}

exports.drawGashapon = async (req, res) => {
    const customerCookies = await transactionModel.sumValuesByUserId(req.session.user_id)
    const gashaponInfo = await gashaponModel.getGashaponByGashaId(req.query.gasha_id)
    if (customerCookies.values < gashaponInfo[0].price) {
        throw "cookies ไม่พอสำหรับการสุ่มกาชาปอง"
    }
    const couponsRate = await couponReqModel.getCouponsRateByGashaId(req.query.gasha_id)
    let sumOfWeights = 0
    for (let i in couponsRate) {
        sumOfWeights = sumOfWeights + couponsRate[i].rate
    }
    let randomNumber = Math.floor(Math.random() * (sumOfWeights + 1))
    let coupon_req_id
    for (let i in couponsRate) {
        randomNumber = randomNumber - couponsRate[i].rate
        if (randomNumber <= 0) {
            coupon_req_id = couponsRate[i].req_id
            break
        }
    }
    const result = await couponReqModel.getCouponInfoByReqId(coupon_req_id, req.query.gasha_id)
    await transactionModel.transactionSystem(req.session.user_id, "draw gashapon", -gashaponInfo[0].price)
    res.json(result)
}

exports.cancelDrawGashapon = async (req, res) => {
    const gashaponInfo = await gashaponModel.getGashaponByGashaId(req.body.gasha_id)
    await transactionModel.transactionSystem(req.session.user_id, "refund", Math.floor(gashaponInfo[0].price * 0.7))
    res.json("การยกเลิกคูปองสำเร็จ")
}

exports.confirmDrawGashapon = async (req, res) => {
    const couponInfo = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    const shop = await couponReqModel.getShopByReqId(req.body.req_id)
    await couponModel.generateCoupon(req.body.req_id, req.session.user_id)
    await transactionModel.transactionSystem(shop.shop_id, "sold coupon", couponInfo.actual_price)
    res.json("การยืนยันคูปองสำเร็จ")
}

exports.getTop3Gasha = async (req, res) => {
    const result = await gashaponModel.getTop3Gasha()
    res.json(result)
}

exports.setTop3Gasha = async (req, res) => {
    const result = await gashaponModel.setTop3Gasha(req.body.first_id, req.body.second_id, req.body.third_id)
    res.json(result)
}
