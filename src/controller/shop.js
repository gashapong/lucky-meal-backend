const userModel = require("../models/user")
const couponReqModel = require("../models/coupon_req")
const transactionModel = require("../models/transaction")
const validator = require("../models/validator")

exports = module.exports = {}

exports.registerShop = async (req, res) => {
    validator.checkShopValidation(req.body.username, req.body.password, req.body.fname, req.body.lname,
        req.body.email, req.body.telephone, req.body.shop_name, req.body.shop_location, req.body.bank_account,
        req.body.bank_name, req.body.account_name)
    const EmailAvailable = await userModel.isEmailAvailable(req.body.email)
    const UserAvailable = await userModel.isUsernameAvailable(req.body.username)
    if (!EmailAvailable) {
        throw "อีเมลนี้มีผู้ใช้งานแล้ว"
    }
    else if (!UserAvailable) {
        throw "ชื่อผู้ใช้นี้มีผู้ใช้งานแล้ว"
    }
    const result = await userModel.createShop(req.body.username, req.body.password, req.body.fname, req.body.lname,
        req.body.email, req.body.telephone, req.body.shop_name, req.body.shop_location, req.body.bank_account,
        req.body.bank_name, req.body.account_name)
    res.json(result)
}

exports.getAllBank = (req, res) => {
    const result = userModel.getAllBank()
    res.json(result)
}

exports.loginShop = async (req, res) => {
    validator.checkUserValidation(req.body.username, req.body.password)
    const result = await userModel.getShopByUsername(req.body.username)
    if (result && (req.body.password === result.password)) {
        req.session.authenticated = true
        req.session.user_id = result.user_id
        req.session.username = result.username
        req.session.user_type = "Shop"
        res.json("เข้าสู่ระบบสำเร็จ")
    }
    else {
        throw "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง"
    }
}

exports.shopInfo = async (req, res) => {
    const resultInfo = await userModel.getShopById(req.session.user_id)
    const resultValues = await transactionModel.sumValuesByUserId(req.session.user_id)
    res.json(Object.assign({}, resultInfo, resultValues))
}

exports.editShopInfo = async (req, res) => {
    validator.checkEditShopValidation(req.body.fname, req.body.lname, req.body.telephone, req.body.shop_name,
        req.body.shop_location, req.body.bank_account, req.body.bank_name, req.body.account_name)
    const result = await userModel.editShopInfo(req.body.fname, req.body.lname, req.body.telephone, req.body.shop_name,
        req.body.shop_location, req.body.bank_account, req.body.bank_name, req.body.account_name, req.session.user_id)
    res.json(result)
}

exports.shopCreateCouponReq = async (req, res) => {
    await validator.checkCouponReqValidation(req.session.user_id, req.body.amount, req.body.food_name, req.body.food_image,
        req.body.food_type, req.body.actual_price, req.body.offer_price, req.body.expiration, req.body.shop_location)
    const result = await couponReqModel.createCouponReq(req.body.amount, req.body.food_name, req.body.food_image,
        req.body.food_type, req.body.actual_price, req.body.offer_price, req.body.expiration, req.body.shop_location, req.session.user_id)
    res.send(result)
}

exports.shopCancelCouponReq = async (req, res) => {
    const coupon_req = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    if (coupon_req && coupon_req.status == "waiting" && coupon_req.shop_id === req.session.user_id) {
        const result = await couponReqModel.deleteCouponReq(req.body.req_id)
        res.send(result)
    } else if (coupon_req && coupon_req.status == "confirm" && coupon_req.shop_id === req.session.user_id) {
        const result = await couponReqModel.changeStatusCouponReq("cancel", req.body.req_id)
        res.send(result)
    } else {
        throw "ไม่สามารถยกเลิกคูปองนี้ได้"
    }
}

exports.shopEditCouponReq = async (req, res) => {
    const coupon_req = await couponReqModel.getCouponReqByReqId(req.body.req_id)
    if (coupon_req && coupon_req.status == "waiting" && coupon_req.shop_id == req.session.user_id) {
        await validator.checkCouponReqValidation(req.session.user_id, req.body.amount, req.body.food_name, req.body.food_image,
            req.body.food_type, req.body.actual_price, req.body.offer_price, req.body.expiration, req.body.shop_location)
        const result = await couponReqModel.editCouponReq(req.body.amount, req.body.food_name, req.body.food_image,
            req.body.food_type, req.body.actual_price, req.body.offer_price, req.body.expiration, req.body.shop_location, req.body.req_id)
        res.send(result)
    }
    else {
        throw "ไม่สามารถแก้ไขคูปองนี้ได้"
    }
}

exports.shopGetCouponReq = async (req, res) => {
    const result = await couponReqModel.getCouponReqByShopId(req.session.user_id)
    const couponWithLoc = await Promise.all(result.map(async (result) => {
        const location = await couponReqModel.getCouponReqLocation(result.req_id)
        return Object.assign({}, result, { location })
    }))
    res.json(couponWithLoc)
}

exports.getShopLocation = async (req, res) => {
    const result = await userModel.getShopLocation(req.session.user_id)
    res.json(result)
}