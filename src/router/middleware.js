const { corsOrigins } = require("../config")

exports = module.exports = {}

exports.requireAuth = (role) => {
    return (req, res, next) => {
        if (!req.session || !req.session.authenticated || req.session.user_type !== role) {
            res.status(403).json("ไม่มีสิทธิ์เข้าถึงส่วนนี้")
            return
        }
        next()
    }
}

exports.enableCors = () => {
    return (req, res, next) => {
        var origin = req.headers.origin
        for (const corsOrigin of corsOrigins) {
            if (corsOrigin === "*" || corsOrigin === origin) {
                res.header("Access-Control-Allow-Origin", origin)
                break
            }
        }
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD")
        res.header("Access-Control-Allow-Credentials", true)
        next()
    }
}