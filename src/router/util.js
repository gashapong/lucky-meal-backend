exports = module.exports = {}

exports.asyncWrapper = (fn) => {
    return async (req, res, next) => {
        try {
            await Promise.resolve(fn(req, res, next))
        } catch (error) {
            console.log(error)
            if(typeof error === "string") {
                res.status(400).json(error)
            } else {
                console.log(error)
                res.status(500).json("Internal Server Error")
            }    
        }
    }
}