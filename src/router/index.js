const express = require("express")
const bodyParser = require("body-parser")
const session = require("express-session")
const swaggerUi = require("swagger-ui-express")
const YAML = require("yamljs")
const morgan = require("morgan")
const authController = require("../controller/auth")
const customerController = require("../controller/customer")
const shopController = require("../controller/shop")
const adminController = require("../controller/admin")
const gashaponController = require("../controller/gashapon")
const couponController = require("../controller/coupon")
const transactionController = require("../controller/transaction")
const { secret } = require("../config")
const { asyncWrapper } = require("./util")
const { requireAuth } = require("./middleware")
const swaggerDocument = YAML.load("./swagger.yaml")

const router = express.Router()

router.use(morgan("common"))
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(session({
    expires: new Date(Date.now() + 3600000), // 1 Hour
    secret,
    resave: true,
    saveUninitialized: true
}))
router.use(swaggerUi.serve)

router.get("/api-docs", swaggerUi.setup(swaggerDocument))

router.post("/shop/register", asyncWrapper(shopController.registerShop))
router.post("/shop/login", asyncWrapper(shopController.loginShop))
router.get("/shop/info", requireAuth("Shop"), asyncWrapper(shopController.shopInfo))
router.put("/shop/info", requireAuth("Shop"), asyncWrapper(shopController.editShopInfo))
router.get("/shop/location", requireAuth("Shop"), asyncWrapper(shopController.getShopLocation))

router.post("/couponReq/cancel", requireAuth("Shop"), asyncWrapper(shopController.shopCancelCouponReq))
router.post("/couponReq", requireAuth("Shop"), asyncWrapper(shopController.shopCreateCouponReq))
router.put("/couponReq", requireAuth("Shop"), asyncWrapper(shopController.shopEditCouponReq))
router.get("/couponReq", requireAuth("Shop"), asyncWrapper(shopController.shopGetCouponReq))
router.get("/couponReq/rate", requireAuth("Admin"), asyncWrapper(gashaponController.getCouponReqRate))

router.get("/coupon", requireAuth("Shop"), asyncWrapper(couponController.customerCouponInfo))
router.delete("/coupon", requireAuth("Shop"), asyncWrapper(couponController.useCoupon))

router.post("/customer/register", asyncWrapper(customerController.registerCustomer))
router.post("/customer/login", asyncWrapper(customerController.loginCustomer))
router.get("/customer/info", requireAuth("Customer"), asyncWrapper(customerController.customerInfo))
router.put("/customer/info", requireAuth("Customer"), asyncWrapper(customerController.editCustomerInfo))
router.get("/customer/coupon", requireAuth("Customer"), asyncWrapper(customerController.getCoupon))

router.get("/cookies", requireAuth("Customer"), asyncWrapper(transactionController.getCredits))
router.post("/topUp", requireAuth("Customer"), asyncWrapper(transactionController.topUp))
router.get("/money", requireAuth("Shop"), asyncWrapper(transactionController.getCredits))
router.post("/paidToshop", requireAuth("Admin"), asyncWrapper(transactionController.paidToShop))
router.get("/payment", requireAuth("Admin"), asyncWrapper(transactionController.getShopPaymentInfo))

router.post("/admin/login", asyncWrapper(adminController.loginAdmin))
router.get("/admin/couponReq", requireAuth("Admin"), asyncWrapper(adminController.getCouponReq))
router.get("/admin/couponReq/all", requireAuth("Admin"), asyncWrapper(adminController.getAllCouponReq))
router.post("/admin/autoClear", requireAuth("Admin"), asyncWrapper(adminController.autoClear))
router.post("/admin/couponReq/accept", requireAuth("Admin"), asyncWrapper(adminController.acceptCouponReq))
router.post("/admin/couponReq/edit", requireAuth("Admin"), asyncWrapper(adminController.editCouponReq))
router.post("/admin/couponReq/reject", requireAuth("Admin"), asyncWrapper(adminController.rejectCouponReq))

router.post("/logout", asyncWrapper(authController.logout))

router.get("/filter", asyncWrapper(gashaponController.filterGashapon))
router.get("/allFoodType", asyncWrapper(gashaponController.getAllFoodType))
router.get("/allLocation", asyncWrapper(gashaponController.getAllLocation))
router.get("/allBank", asyncWrapper(shopController.getAllBank))

router.get("/gashapon/search", asyncWrapper(gashaponController.searchGashapon))
router.get("/gashapon/canDraw", requireAuth("Customer"), asyncWrapper(gashaponController.canDraw))
router.get("/gashapon/draw", requireAuth("Customer"), asyncWrapper(gashaponController.drawGashapon))
router.post("/gashapon/draw/confirm", requireAuth("Customer"), asyncWrapper(gashaponController.confirmDrawGashapon))
router.post("/gashapon/draw/cancel", requireAuth("Customer"), asyncWrapper(gashaponController.cancelDrawGashapon))
router.get("/gashapon/coupon", asyncWrapper(gashaponController.couponsInGashapon))
router.get("/gashapon/top", asyncWrapper(gashaponController.getTop3Gasha))
router.put("/gashapon/top", requireAuth("Admin"), asyncWrapper(gashaponController.setTop3Gasha))

module.exports = router
