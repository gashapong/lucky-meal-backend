const dotenv = require("dotenv")

dotenv.config()

module.exports = {
    port: process.env.HTTP_PORT || 3210,
    db: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
    },
    corsOrigins: process.env.CORS_ORIGIN,
    secret: process.env.SECRET || "development_secret",
    get baseUrl() {
        return process.env.BASE_URL || `http://localhost:${this.port}`
    }
}
