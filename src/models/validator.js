const validator = require("validator")
const gashaponModel = require("../models/gashapon")
const userModel = require("../models/user")
const isimgUrl = require("is-image-url")

exports = module.exports = {}

const re = new RegExp("^([A-Z]|[a-z]|[ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝ๑๒๓๔ูฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ])+$")
const dateFormat = new RegExp(/^\d{2}\/\d{2}$/)

const isShopLocationList = (locationList, shopLocation) => {
    for (const location of locationList.split(",")) {
        if (!shopLocation.includes(location)) {
            return false
        }
    }
    return true
}

exports.checkUserValidation = (username, password) => {
    username = username.toString()
    password = password.toString()
    if (!validator.isAlphanumeric(username) || !validator.isLength(username, { min: 4, max: 32 })) {
        throw "ชื่อผู้ใช้ต้องเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 4-32 ตัวอักษร"
    }
    else if (!validator.isAlphanumeric(password) || !validator.isLength(password, { min: 6, max: 32 })) {
        throw "รหัสผ่านเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 6-32 ตัวอักษร"
    }
}

exports.checkCustomerValidation = (username, password, fname, lname, email, telephone) => {
    username = username.toString()
    password = password.toString()
    fname = fname.toString()
    lname = lname.toString()
    email = email.toString()
    telephone = telephone.toString()
    if (!validator.isAlphanumeric(username) || !validator.isLength(username, { min: 4, max: 32 })) {
        throw "่ชื่อผู้ใช้ต้องเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 4-32 ตัวอักษร"
    }
    else if (!validator.isAlphanumeric(password) || !validator.isLength(password, { min: 6, max: 32 })) {
        throw "รหัสผ่านเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 6-32 ตัวอักษร"
    }
    else if (!re.test(fname) || !validator.isLength(fname, { min: 2, max: 32 })) {
        throw "ชื่อต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!re.test(lname) || !validator.isLength(lname, { min: 2, max: 32 })) {
        throw "นามสกุลต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!validator.isEmail(email) || !validator.isLength(email, { min: 8, max: 320 })) {
        throw "รูปแบบอีเมลไม่ถูกต้อง"
    }
    else if (!validator.isMobilePhone(telephone, "th-TH") || !validator.isLength(telephone, { min: 10, max: 10 })) {
        throw "เบอร์ติดต่อต้องเป็นตัวเลข 10 หลัก ไม่มีอักษรพิเศษ"
    }
}

exports.checkEditCustomerValidation = (fname, lname, telephone) => {
    fname = fname.toString()
    lname = lname.toString()
    telephone = telephone.toString()
    if (!re.test(fname) || !validator.isLength(fname, { min: 2, max: 32 })) {
        throw "ชื่อต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!re.test(lname) || !validator.isLength(lname, { min: 2, max: 32 })) {
        throw "นามสกุลต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!validator.isMobilePhone(telephone, "th-TH") || !validator.isLength(telephone, { min: 10, max: 10 })) {
        throw "เบอร์ติดต่อต้องเป็นตัวเลข 10 หลัก ไม่มีอักษรพิเศษ"
    }
}

exports.checkShopValidation = (
    username, password, fname, lname,
    email, telephone, shop_name, shop_location,
    bank_account, bank_name, account_name
) => {
    username = username.toString()
    password = password.toString()
    fname = fname.toString()
    lname = lname.toString()
    email = email.toString()
    telephone = telephone.toString()
    shop_name = shop_name.toString()
    shop_location = shop_location.toString()
    bank_account = bank_account.toString()
    bank_name = bank_name.toString()
    account_name = account_name.toString()
    const shopLocation = gashaponModel.getAllLocation()
    const bankName = userModel.getAllBank()
    if (!validator.isAlphanumeric(username) || !validator.isLength(username, { min: 4, max: 32 })) {
        throw "่ชื่อผู้ใช้ต้องเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 4-32 ตัวอักษร"
    }
    else if (!validator.isAlphanumeric(password) || !validator.isLength(password, { min: 6, max: 32 })) {
        throw "รหัสผ่านเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 6-32 ตัวอักษร"
    }
    else if (!re.test(fname) || !validator.isLength(fname, { min: 2, max: 32 })) {
        throw "ชื่อต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!re.test(lname) || !validator.isLength(lname, { min: 2, max: 32 })) {
        throw "นามสกุลต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!validator.isEmail(email) || !validator.isLength(email, { min: 8, max: 320 })) {
        throw "รูปแบบอีเมลไม่ถูกต้อง"
    }
    else if (!validator.isMobilePhone(telephone, "th-TH") || !validator.isLength(telephone, { min: 10, max: 10 })) {
        throw "เบอร์ติดต่อต้องเป็นตัวเลข 10 หลัก ไม่มีอักษรพิเศษ"
    }
    else if (!shop_name.split(" ").every(function (word) { return re.test(word) }) || !validator.isLength(shop_name, { min: 2, max: 64 })) {
        throw "ชื่อร้านอาหารต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!isShopLocationList(shop_location, shopLocation)) {
        throw "โซนร้านอาหารไม่อยู่ในบริเวณที่กำหนด"
    }
    else if (!account_name.split(" ").every(function (word) { return re.test(word) }) || !validator.isLength(account_name, { min: 2, max: 64 })) {
        throw "ชื่อบัญชีธนาคารต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!validator.isNumeric(bank_account) || !validator.isLength(bank_account, { min: 4, max: 16 })) {
        throw "เลขบัญชีธนาคารไม่ถูกต้อง"
    }
    else if (!bankName.includes(bank_name)) {
        throw "ชื่อบัญชีธนาคารไม่ถูกต้อง"
    }
}

exports.checkEditShopValidation = (
    fname, lname, telephone, shop_name, shop_location,
    bank_account, bank_name, account_name
) => {
    fname = fname.toString()
    lname = lname.toString()
    telephone = telephone.toString()
    shop_name = shop_name.toString()
    shop_location = shop_location.toString()
    bank_account = bank_account.toString()
    bank_name = bank_name.toString()
    account_name = account_name.toString()
    const shopLocation = gashaponModel.getAllLocation()
    const bankName = userModel.getAllBank()
    if (!re.test(fname) || !validator.isLength(fname, { min: 2, max: 32 })) {
        throw "ชื่อผู้ใช้ต้องเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 4-32 ตัวอักษร"
    }
    else if (!re.test(lname) || !validator.isLength(lname, { min: 2, max: 32 })) {
        throw "รหัสผ่านเป็นตัวอักษรอังกฤษหรือตัวเลขที่มีความยาว 6-32 ตัวอักษร"
    }
    else if (!validator.isMobilePhone(telephone, "th-TH") || !validator.isLength(telephone, { min: 10, max: 10 })) {
        throw "เบอร์ติดต่อต้องเป็นตัวเลข 10 หลัก ไม่มีอักษรพิเศษ"
    }
    else if (!shop_name.split(" ").every(function (word) { return re.test(word) }) || !validator.isLength(shop_name, { min: 2, max: 64 })) {
        throw "ชื่อร้านอาหารต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!isShopLocationList(shop_location, shopLocation)) {
        throw "โซนร้านอาหารไม่อยู่ในบริเวณที่กำหนด"
    }
    else if (!account_name.split(" ").every(function (word) { return re.test(word) }) || !validator.isLength(account_name, { min: 2, max: 64 })) {
        throw "ชื่อบัญชีธนาคารต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!validator.isNumeric(bank_account) || !validator.isLength(bank_account, { min: 4, max: 16 })) {
        throw "เลขบัญชีธนาคารไม่ถูกต้อง"
    }
    else if (!bankName.includes(bank_name)) {
        throw "ชื่อบัญชีธนาคารไม่อยู่ในที่กำหนดไว้"
    }
}

exports.checkCouponReqValidation = async (
    user_id, amount, food_name, food_image,
    food_type, actual_price, offer_price,
    expiration, shop_location
) => {
    amount = amount.toString()
    food_name = food_name.toString()
    food_image = food_image.toString()
    food_type = food_type.toString()
    actual_price = actual_price.toString()
    offer_price = offer_price.toString()
    expiration = expiration.toString()
    shop_location = shop_location.toString()
    const foodType = gashaponModel.getAllFoodType()
    const shopLocation = await userModel.getShopLocation(user_id)
    console.log(validator.isAfter(expiration))
    if (!validator.isNumeric(amount) || !validator.isLength(amount, { min: 1, max: 6 })) {
        throw "จำนวนคูปองต้องเป็นตัวเลขเท่านั้น"
    }
    else if (!food_name.split(" ").every(function (word) { return re.test(word) }) || !validator.isLength(food_name, { min: 2, max: 128 })) {
        throw "ชื่ออาหารต้องเป็นตัวอักษรอังกฤษหรือไทย"
    }
    else if (!isimgUrl(food_image)) {
        throw "รูปแบบ url ของภาพไม่ถูกต้อง"
    }
    else if (!foodType.includes(food_type)) {
        throw "ชนิดอาหารไม่อยู่ในที่กำหนดไว้"
    }
    else if (!validator.isNumeric(actual_price) || !validator.isLength(actual_price, { min: 1, max: 4 })) {
        throw "ราคาจริงต้องเป็นตัวเลขเท่านั้น"
    }
    // for convert to string
    // else if (!validator.isNumeric(String(offer_price)) || !validator.isLength(String(offer_price), { min: 1, max: 4 })) {
    else if (!validator.isNumeric(offer_price) || !validator.isLength(offer_price, { min: 1, max: 4 })) {
        throw "ราคาเสนอขายต้องเป็นตัวเลขเท่านั้น"
    }
    else if (!validator.isAfter(expiration)) {
        throw "กรุณาตรวจสอบวันหมดอายุ"
    }
    else if (!isShopLocationList(shop_location, shopLocation)) {
        throw "โซนร้านอาหารไม่อยู่ในโซนที่ร้านอยู่"
    }
}

exports.checkTopUpValidation = (card_number, expiration, cvc) => {
    card_number = card_number.toString()
    expiration = expiration.toString()
    cvc = cvc.toString()
    if (!validator.isNumeric(card_number) || !validator.isLength(card_number, { min: 16, max: 16 })) {
        throw "หมายเลขบัตรต้องเป็นตัวเลข 16 หลัก ไม่มีอักษรพิเศษ"
    }
    if (dateFormat.test(expiration)) {
        const date = expiration.split("/")
        const mm = date[0]
        const yy = date[1]
        if (!validator.toDate(mm + "/01/" + yy)) {
            throw "วันหมดอายุไม่ถูกต้อง"
        }
    } else {
        throw "วันหมดอายุต้องอยู่ในรูปแบบ ดด/ปป"
    }
    if (!validator.isNumeric(cvc) || !validator.isLength(cvc, { min: 3, max: 4 })) {
        throw "CVC ต้องเป็นตัวเลข 3-4 ตัว"
    }
}
