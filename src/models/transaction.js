const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

const getTransactionType = async (con, description) => {
    try {
        const result = await con.query("SELECT * FROM `transaction_type` WHERE `desc` = ?", [description])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีระบบการเงินประเภทนี้ในระบบ"
    }
}

const createTransactionType = async (con, description) => {
    try {
        const result = con.query("INSERT INTO `transaction_type`(`desc`) VALUES (?)", [description])
        return { type_id: result.insertId }
    } catch (error) {
        throw "ไม่สามารถเพิ่มประเภทของระบบการเงินได้"
    }
}

const createTransaction = async (con, value, user_id, type_id) => {
    try {
        await con.query("INSERT INTO transaction(`value`, `user_id`, `type_id`) VALUES (?, ?, ?)", [value, user_id, type_id])
        return "การเพิ่มข้อมูลในระบบการเงินสำเร็จ"
    } catch (error) {
        throw "การเพิ่มข้อมูลในระบบการเงินล้มเหลว"
    }
}

exports.transactionSystem = async (user_id, description, value) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        let typeResult = await getTransactionType(con, description)
        if (!typeResult) {
            typeResult = await createTransactionType(con, description)
        }
        const result = await createTransaction(con, value, user_id, typeResult.type_id)
        await con.commit()
        return result
    } catch (error) {
        await con.rollback()
        throw error
    } finally {
        con.release()
    }
}

exports.sumValuesByUserId = async (user_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT SUM(`value`) AS `values` FROM `transaction` WHERE user_id = ?", [user_id])
        if (result[0].values === null) {
            return { values: 0 }
        }
        return result[0]
    } catch (error) {
        throw "ไม่สามารถคำนวณเงินในระบบได้"
    } finally {
        con.release()
    }
}
