const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

exports.generateCoupon = async (req_id, cus_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        await con.query("INSERT INTO `coupon`(`coupon_id`, `req_id`, `cus_id`) VALUES (uuid(), ?, ?);", [req_id, cus_id])
        await con.query("update coupon_req set sold_out_amount = sold_out_amount + 1 where req_id = ?", [req_id])
        await con.query("UPDATE coupon_req SET status = 'end' WHERE req_id = ? and total_amount = sold_out_amount", [req_id])
        await con.commit()
        return "ระบบสร้างคูปองสำเร็จ"
    } catch (error) {
        await con.rollback()
        throw "ระบบสร้างคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.getCouponsByCusId = async (cus_id) => {
    const con = await getConnection()
    try {
        await con.query(`
            DELETE FROM coupon WHERE req_id IN (SELECT req_id FROM coupon_req WHERE cus_id = ? AND expiration < now())
            `, [cus_id])
        const result = await con.query(`
            SELECT req_id, coupon_id, food_name, food_image, actual_price, expiration, shop_name
            FROM coupon NATURAL JOIN coupon_req NATURAL JOIN shop
            WHERE cus_id = ? and expiration >= now()
            `, [cus_id])
        return result
    } catch (error) {
        throw "ไม่มีคูปองนี้ในระบบ"
    } finally {
        con.release()
    }
}

exports.getCustomerCoupon = async (coupon_id) => {
    const con = await getConnection()
    try {
        const result = await con.query(
            `SELECT req_id, coupon_id, food_name, food_image, actual_price, expiration, shop_name
            FROM coupon NATURAL JOIN coupon_req NATURAL JOIN shop 
            WHERE coupon_id = ?`, [coupon_id])
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่มีคูปองนี้ในระบบ"
    } finally {
        con.release()
    }
}

exports.deleteCoupon = async (coupon_id) => {
    const con = await getConnection()
    try {
        con.beginTransaction()
        const coupon = await exports.getCustomerCoupon(coupon_id)
        await con.query("DELETE FROM `coupon` WHERE `coupon_id` = ?", [coupon_id])
        await con.query("update coupon_req set used_amount = used_amount + 1 where req_id = ?", [coupon[0].req_id])
        con.commit()
        return "การใช้คูปองสำเร็จ"
    } catch (error) {
        con.rollback()
        throw "ไม่สามารถใช้คูปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.isCouponAvailable = async (coupon_id, shop_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM coupon NATURAL JOIN coupon_req WHERE coupon_id = ? and expiration >= now()", [coupon_id])
        if (result.length === 0) {
            throw "ไม่มีคูปองนี้ในระบบ"
        }
        else if (result[[0]].shop_id != shop_id) {
            throw "ไม่มีสิทธิ์ดูข้อมูลของคูปองนี้ได้"
        }

        return true
    } catch (error) {
        throw error
    } finally {
        con.release()
    }
}