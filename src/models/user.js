const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

const bank_name = ["ธนาคารกรุงเทพ", "ธนาคารกสิกรไทย", "ธนาคารไทยพาณิชย์", "ธนาคารกรุงไทย", "ธนาคารกรุงศรีอยุธยา"]

exports.getAllBank = () => {
    return bank_name
}

exports.isEmailAvailable = async (email) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` WHERE `email` = ?", [email])
        if (result.length === 0) {
            return true
        }
        return false
    } catch (error) {
        throw "อีเมลนี้มีผู้ใช้งานแล้ว"
    } finally {
        con.release()
    }
}

exports.isUsernameAvailable = async (username) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` WHERE `username` = ?", [username])
        if (result.length === 0) {
            return true
        }
        return false
    } catch (error) {
        throw "ชื่อผู้ใช้นี้มีผู้ใช้งานแล้ว"
    } finally {
        con.release()
    }
}

exports.createCustomer = async (username, password, fname, lname, email, telephone) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        const userResult = await con.query("INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES (?, ?, ?, ?, ?, ?)", [email, username, password, fname, lname, telephone])
        await con.query("INSERT INTO `customer`(`cus_id`) VALUES (?)", [userResult.insertId])
        await con.commit()
        return "การลงทะเบียนเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การลงทะเบียนล้มเหลว"
    } finally {
        con.release()
    }
}

exports.createShop = async (
    username, password, fname, lname,
    email, telephone, shop_name, shop_location,
    bank_account, bank_name, account_name
) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        const userResult = await con.query("INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES (?, ?, ?, ?, ?, ?)", [email, username, password, fname, lname, telephone])
        await con.query("INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES (?, ?, ?, ?, ?)", [userResult.insertId, shop_name, bank_name, bank_account, account_name])
        for (const location of shop_location) {
            await con.query("INSERT INTO shop_location(shop_id,location) VALUES (?, ?)", [userResult.insertId, location])
        }
        await con.commit()
        return "การลงทะเบียนเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การลงทะเบียนล้มเหลว"
    } finally {
        con.release()
    }
}

exports.getCustomerById = async (id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` JOIN `customer` on `cus_id` = `user_id` WHERE `cus_id` = ?", [id])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีข้อมูลของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}

exports.getCustomerByUsername = async (username) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` JOIN `customer` on `cus_id` = `user_id` WHERE `username` = ?", [username])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีข้อมูลของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}

exports.getAllShop = async () => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM shop")
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่สามารถดูข้อมูลร้านค้าในระบบได้"
    } finally {
        con.release()
    }
}

exports.getShopById = async (id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` JOIN `shop` on `shop_id` = `user_id` WHERE `shop_id` = ?", [id])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีข้อมูลของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}

exports.getShopByUsername = async (username) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `user` JOIN `shop` ON `shop_id` = `user_id` WHERE `username` = ?", [username])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีข้อมูลของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}

exports.getShopLocation = async (shop_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT `location` FROM `shop_location` WHERE `shop_id` = ?", [shop_id])
        if (result.length === 0) {
            return null
        }
        return result.map((res) => { return res.location })
    } catch (error) {
        throw "ไม่มีข้อมูลโซนร้านอาหารของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}

exports.editCustomerInfo = async (fname, lname, telephone, user_id) => {
    const con = await getConnection()
    try {
        await con.query("UPDATE `user` SET `fname` = ?, `lname` = ?, `telephone` = ? WHERE user_id = ?", [fname, lname, telephone, user_id])
        return "ข้อมูลได้รับการแก้ไขเสร็จเรียบร้อย"
    } catch (error) {
        throw "การแก้ไขข้อมูลล้มเหลว"
    } finally {
        con.release()
    }
}

exports.editShopInfo = async (
    fname, lname, telephone, shop_name, shop_location,
    bank_account, bank_name, account_name, user_id
) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        await con.query("UPDATE `user` SET `fname` = ?, `lname` = ?, `telephone` = ? WHERE `user_id` = ?", [fname, lname, telephone, user_id])
        await con.query("UPDATE shop SET shop_name = ?, bank_name = ?, bank_account = ?, account_name = ? WHERE shop_id = ?", [shop_name, bank_name, bank_account, account_name, user_id])
        await con.query("DELETE FROM shop_location WHERE shop_id = ?", [user_id])
        for (const location of shop_location) {
            await con.query("INSERT INTO shop_location(shop_id,location) VALUES (?, ?)", [user_id, location])
        }
        await con.commit()
        return "ข้อมูลได้รับการแก้ไขเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การแก้ไขข้อมูลล้มเหลว"
    } finally {
        con.release()
    }
}
