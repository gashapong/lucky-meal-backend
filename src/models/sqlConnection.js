const mysql = require("mysql")
const util = require("util")
const { db } = require("../config")

const pool = mysql.createPool(db)

exports = module.exports = {}

exports.getConnection = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if (err) return reject(err)
            conn.query = util.promisify(conn.query)
            conn.beginTransaction = util.promisify(conn.beginTransaction)
            conn.commit = util.promisify(conn.commit)
            conn.rollback = util.promisify(conn.rollback)
            resolve(conn)
        })
    })
}
