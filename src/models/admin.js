const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

exports.getAdminByUsername = async (username) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `admin` WHERE `username` = ?", [username])
        if (result.length === 0) {
            return null
        }
        return result[0]
    } catch (error) {
        throw "ไม่มีข้อมูลของผู้ใช้ระบบ"
    } finally {
        con.release()
    }
}
