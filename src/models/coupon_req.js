const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

exports.getAllCouponReq = async () => {
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT req_id, food_name, food_image, status, total_amount, food_type, shop_name
            FROM coupon_req NATURAL JOIN shop ORDER BY -req_id`)
        return result
    } catch (error) {
        throw "ไม่สามารถดูคูปองทั้งหมดได้"
    } finally {
        con.release()
    }
}

exports.getCouponsInfoByGashaId = async (gasha_id) => {
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT food_name, food_image, actual_price, shop_name, rarity
            FROM include_coupon NATURAL JOIN coupon_req NATURAL JOIN shop
            WHERE gasha_id = ?
            ORDER BY rarity
            `, [gasha_id])
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่มีคูปองในตู้กาชาปองนี้"
    } finally {
        con.release()
    }
}

exports.getCouponsRateByGashaId = async (gasha_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT req_id, food_name, food_image, actual_price, offer_price, rate, rarity FROM include_coupon natural join coupon_req WHERE gasha_id = ?", [gasha_id])
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่สามารถค้นหาคูปองในตู้กาชาปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getCouponsRateByReqId = async (req_id) => {
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT price, rate, rarity 
            FROM coupon_req NATURAL JOIN include_coupon NATURAL JOIN gasha 
            WHERE req_id = ?
            `, [req_id])
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่สามารถค้นหาคูปองในตู้กาชาปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getCouponInfoByReqId = async (req_id, gasha_id) => {
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT req_id, food_name, food_image, actual_price, shop_name, rarity
            FROM include_coupon NATURAL JOIN coupon_req NATURAL JOIN shop 
            WHERE req_id = ? and gasha_id = ?
            `, [req_id, gasha_id])
        if (result.length === 0) {
            return null
        }
        return result
    } catch (error) {
        throw "ไม่สามารถค้นหาคูปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getCouponReqByReqId = async (req_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `coupon_req` WHERE `req_id` = ?", [req_id])
        return result[0]
    } catch (error) {
        throw "ไม่สามารถค้นหาคูปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getCouponReqByShopId = async (shop_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM `coupon_req` WHERE `shop_id` = ?", [shop_id])
        return result
    } catch (error) {
        throw "ไม่สามารถดูคูปองทั้งหมดได้"
    } finally {
        con.release()
    }
}

exports.getShopByReqId = async (req_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT shop_id, shop_name FROM `coupon_req` NATURAL JOIN `shop` WHERE `req_id` = ?", [req_id])
        return result[0]
    } catch (error) {
        throw "ไม่สามารถค้นหาร้านอาหารของคูปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getCouponReqLocation = async (req_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT `food_location` FROM `coupon_location` WHERE `req_id` = ?", [req_id])
        if (result.length === 0) {
            return null
        }
        return result.map((res) => { return res.food_location })
    } catch (error) {
        throw "ไม่สามารถค้นหาโซนร้านอาหารของคูปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.createCouponReq = async (amount, food_name, food_image, food_type, actual_price, offer_price, expiration, shop_location, shop_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        const couponReqResult = await con.query(`
            INSERT INTO coupon_req(status, total_amount, sold_out_amount, used_amount, food_name, food_image, food_type, actual_price, offer_price, expiration, shop_id) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            `, ["waiting", amount, 0, 0, food_name, food_image, food_type, actual_price, offer_price, expiration, shop_id])
        for (const location of shop_location) {
            await con.query("INSERT INTO coupon_location(req_id,food_location) VALUES (?, ?)", [couponReqResult.insertId, location])
        }
        await con.commit()
        return "การสร้างคูปองเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การสร้างคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.deleteCouponReq = async (req_id) => {
    const con = await getConnection()
    try {
        await con.query("DELETE FROM `coupon_req` WHERE `req_id` = ?", [req_id])
        return "การยกเลิกคูปองสำเร็จ"
    } catch (error) {
        throw "การยกเลิกคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.editCouponReq = async (amount, food_name, food_image, food_type, actual_price, offer_price, expiration, shop_location, req_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        await con.query("UPDATE `coupon_req` SET `total_amount` = ?, `food_name` = ? ,`food_image` = ? ,`food_type` = ? ,`actual_price` = ? ,`offer_price` = ? ,`expiration` = ? WHERE `req_id` = ?", [amount, food_name, food_image, food_type, actual_price, offer_price, expiration, req_id])
        await con.query("DELETE FROM coupon_location WHERE req_id = ?", [req_id])
        for (const location of shop_location) {
            await con.query("INSERT INTO coupon_location(req_id,food_location) VALUES (?, ?)", [req_id, location])
        }
        await con.commit()
        return "การแก้ไขคูปองเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การแก้ไขคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.changeStatusCouponReq = async (status, req_id) => {
    const con = await getConnection()
    try {
        await con.query("UPDATE `coupon_req` SET `status` = ? WHERE `req_id` = ?", [status, req_id])
        return "การเปลี่ยนสถานะคูปองสำเร็จ"
    } catch (error) {
        throw "การเปลี่ยนสถานะคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.acceptCouponReq = async (rate_include, gasha_include, req_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        for (let key in gasha_include) {
            for (let gasha_id of gasha_include[key]) {
                await con.query("INSERT INTO `include_coupon`(`gasha_id`, `req_id`, `rarity`, `rate`) VALUES (?, ?, ?, ?)"
                    , [gasha_id, req_id, rate_include[key].rarity, rate_include[key].rate])
            }
        }
        con.query("UPDATE `coupon_req` SET `status` = ? WHERE `req_id` = ?", ["confirm", req_id])
        await con.commit()
        return "การอนุมัติคูปองสำเร็จ"
    } catch (error) {
        await con.rollback()
        throw "การอนุมัติคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.editCouponReqRate = async (rate_include, gasha_include, req_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        await con.query("DELETE FROM include_coupon WHERE req_id = ?", [req_id])
        for (let key in gasha_include) {
            for (let gasha_id of gasha_include[key]) {
                await con.query("INSERT INTO `include_coupon`(`gasha_id`, `req_id`, `rarity`, `rate`) VALUES (?, ?, ?, ?)"
                    , [gasha_id, req_id, rate_include[key].rarity, rate_include[key].rate])
            }
        }
        await con.commit()
        return "การแก้ไขคูปองสำเร็จ"
    } catch (error) {
        await con.rollback()
        throw "การแก้ไขคูปองล้มเหลว"
    } finally {
        con.release()
    }
}

exports.autoClear = async () => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        //update expired coupon -> end
        await con.query("UPDATE coupon_req SET status = 'end' WHERE expiration <= now()")
        //update cancel -> end
        await con.query("UPDATE coupon_req SET status = 'end' WHERE status = 'cancel'")
        //delete include_coupon if status = end
        await con.query("DELETE FROM include_coupon WHERE req_id IN (SELECT req_id FROM coupon_req WHERE status = 'end')")
        await con.commit()
        return "การจัดการคูปองในตู้อัตโนมัติสำเร็จ"
    } catch (error) {
        await con.rollback()
        throw "การจัดการคูปองในตู้อัตโนมัติล้มเหลว"
    } finally {
        con.release()
    }
}
