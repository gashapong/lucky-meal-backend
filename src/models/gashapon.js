const { getConnection } = require("./sqlConnection")

exports = module.exports = {}

const food_type = ["อาหารญี่ปุ่น", "ของหวาน+เครื่องดื่ม", "อาหารไทย", "ฟาสต์ฟู้ด"]
const location = ["สยาม", "จามจุรีสแควร์", "สวนหลวง", "สามย่าน"]
const price = [10, 19, 28, 37]

const gasha_filter = {
    location: location,
    price: price,
    type: food_type
}


exports.getAllFoodType = () => {
    return food_type
}

exports.getAllLocation = () => {
    return location
}

exports.getGashaponFilter = () => {
    return gasha_filter
}

exports.searchGashapon = async (location, type, price) => {
    location = location || "%"
    type = type || "%"
    price = price || "%"
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT * FROM gasha
            WHERE location LIKE ? AND type LIKE ? AND price LIKE ?
            `, [location, type, price])
        return result
    } catch (error) {
        throw "ไม่สามารถค้นหาตู้กาชาปองได้"
    } finally {
        con.release()
    }
}

exports.searchAviableGashapon = async (location, type, price) => {
    location = location || "%"
    type = type || "%"
    price = price || "%"
    const con = await getConnection()
    try {
        const result = await con.query(`
            SELECT gasha.gasha_id, type, location, price
            FROM include_coupon LEFT JOIN gasha
            ON include_coupon.gasha_id = gasha.gasha_id
            WHERE location LIKE ? AND type LIKE ? AND price LIKE ?
            GROUP BY gasha.gasha_id
            `, [location, type, price])
        return result
    } catch (error) {
        throw "ไม่สามารถค้นหาตู้กาชาปองนี้ได้"
    } finally {
        con.release()
    }
}

exports.getTop3Gasha = async () => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM gasha WHERE top_gasha = 1")
        return result
    } catch (error) {
        return "ไม่สามารถดูข้อมูลตู้กาชาปองแนะนำ 3 อันดับได้"
    } finally {
        con.release()
    }
}

exports.setTop3Gasha = async (first_id, second_id, third_id) => {
    const con = await getConnection()
    try {
        await con.beginTransaction()
        const gashapons = await getAllGashapon()
        let i
        for (i = 0; i < gashapons.length; i++) {
            const gasha_id = gashapons[i].gasha_id
            if (gasha_id == first_id || gasha_id == second_id || gasha_id == third_id) {
                await con.query("UPDATE gasha SET top_gasha = 1 WHERE gasha_id = ?", [gasha_id])
            }
            else {
                await con.query("UPDATE gasha SET top_gasha = 0 WHERE gasha_id = ?", [gasha_id])
            }
        }
        await con.commit()
        return "การแก้ไขตู้กาชาปองแนะนำเสร็จเรียบร้อย"
    } catch (error) {
        await con.rollback()
        throw "การแก้ไขตู้กาชาปองแนะนำล้มเหลว"
    } finally {
        con.release()
    }
}

const getAllGashapon = async () => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM gasha")
        return result
    } catch (error) {
        throw "ไม่มีตู้กาชาปอง"
    } finally {
        con.release()
    }
}

exports.getGashaponByGashaId = async (gasha_id) => {
    const con = await getConnection()
    try {
        const result = await con.query("SELECT * FROM gasha WHERE gasha_id = ?", [gasha_id])
        return result
    } catch (error) {
        throw "ไม่มีตู้กาชาปอง"
    } finally {
        con.release()
    }
}
