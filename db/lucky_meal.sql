CREATE DATABASE IF NOT EXISTS `db_lucky_meal` CHARACTER SET UTF8MB4 COLLATE=UTF8MB4_UNICODE_CI;
USE `db_lucky_meal`;

CREATE TABLE `user` (
`user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`email` VARCHAR(150) NOT NULL UNIQUE,
`username` VARCHAR(32) NOT NULL UNIQUE,
`password` VARCHAR(32) NOT NULL,
`fname` VARCHAR(32) NOT NULL,
`lname` VARCHAR(32) NOT NULL,
`telephone` VARCHAR(10),
PRIMARY KEY(`user_id`)
);

CREATE TABLE `customer` (
`cus_id` INT UNSIGNED NOT NULL,
PRIMARY KEY(`cus_id`),
FOREIGN KEY (`cus_id`) REFERENCES `user`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE `shop` (
`shop_id` INT UNSIGNED NOT NULL,
`shop_name` VARCHAR(64) NOT NULL,
`bank_name` VARCHAR(32) NOT NULL,
`bank_account` VARCHAR(16) NOT NULL,
`account_name` VARCHAR(64) NOT NULL,
PRIMARY KEY(`shop_id`),
FOREIGN KEY (`shop_id`) REFERENCES `user`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE `shop_location` (
`location` VARCHAR(32) NOT NULL,
`shop_id` INT UNSIGNED NOT NULL,
CONSTRAINT pk_shopLocation PRIMARY KEY (`location`, `shop_id`),
FOREIGN KEY (`shop_id`) REFERENCES `shop`(`shop_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `coupon_req` (
`req_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`status` ENUM('waiting', 'confirm', 'reject', 'cancel', 'end') NOT NULL,
`req_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
`total_amount` INT UNSIGNED NOT NULL,
`sold_out_amount` INT UNSIGNED NOT NULL CHECK (`sold_out_amount` <= `total_amount`),
`used_amount` INT UNSIGNED NOT NULL CHECK (`used_amount` <= `total_amount`),
`food_name` VARCHAR(128) NOT NULL,
`food_image` TEXT,
`food_type` VARCHAR(32) NOT NULL,
`actual_price` DOUBLE UNSIGNED NOT NULL,
`offer_price` DOUBLE UNSIGNED NOT NULL,
`expiration` DATE NOT NULL,
`shop_id` INT UNSIGNED NOT NULL,
PRIMARY KEY (`req_id`),
FOREIGN KEY (`shop_id`) REFERENCES `shop`(`shop_id`) ON UPDATE CASCADE
);

CREATE TABLE `coupon_location` (
`req_id` INT UNSIGNED NOT NULL,
`food_location` VARCHAR(32) NOT NULL,
CONSTRAINT pk_couponLocation PRIMARY KEY (`food_location`, `req_id`),
FOREIGN KEY (`req_id`) REFERENCES `coupon_req`(`req_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `coupon` (
`coupon_id` CHAR(36) NOT NULL,
`req_id` INT UNSIGNED NOT NULL,
`cus_id` INT UNSIGNED NOT NULL,
PRIMARY KEY (`coupon_id`),
FOREIGN KEY (`req_id`) REFERENCES `coupon_req`(`req_id`) ON UPDATE CASCADE,
FOREIGN KEY (`cus_id`) REFERENCES `customer`(`cus_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `gasha` (
`gasha_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`type` VARCHAR(32) NOT NULL,
`location` VARCHAR(32) NOT NULL,
`price` INT UNSIGNED NOT NULL,
`top_gasha` BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY (`gasha_id`)
);

CREATE TABLE `include_coupon` (
`gasha_id` INT UNSIGNED NOT NULL,
`req_id` INT UNSIGNED NOT NULL,
`rarity` ENUM('SSR', 'SR', 'R', 'C') NOT NULL,
`rate` DOUBLE UNSIGNED NOT NULL,
CONSTRAINT pk_gashaCoupon PRIMARY KEY (gasha_id, req_id),
FOREIGN KEY (`gasha_id`) REFERENCES `gasha`(`gasha_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`req_id`) REFERENCES `coupon_req`(`req_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `transaction_type` (
`type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`desc` VARCHAR(32) NULL UNIQUE,
PRIMARY KEY (`type_id`)
);

CREATE TABLE `transaction` (
`trans_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`value` DOUBLE NOT NULL,
`user_id` INT UNSIGNED NOT NULL,
`type_id` INT UNSIGNED NOT NULL,
PRIMARY KEY (`trans_id`),
FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`type_id`) REFERENCES `transaction_type`(`type_id`) ON UPDATE CASCADE
);

CREATE TABLE `admin` (
`admin_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`username` VARCHAR(32) NOT NULL UNIQUE,
`password` VARCHAR(32) NOT NULL,
PRIMARY KEY (`admin_id`)
);