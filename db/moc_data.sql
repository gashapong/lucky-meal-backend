use db_lucky_meal;

INSERT INTO `transaction_type`(`desc`) VALUES 
('top up'), 
('draw gashapon'), 
('refund'),
('sold coupon'),
('compensation');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('udonkitwatra@ptp.com', 'seoulcialclub', '1234567890', 'นายอุดร', 'กิจวัตร', '0826072492');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Fuji', 'Krungthai Bank', '1262094650', 'นายอุดร กิจวัตร');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม'), (LAST_INSERT_ID(), 'จามจุรีสแควร์'), (LAST_INSERT_ID(), 'สวนหลวง');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('anuluck@ptp.com', 'naekisushi', '1234567890', 'นางสาวอนุรักษ์', 'กิจวัตร', '0821062510');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Moma bubble tea bar', 'TMB Bank', '2889990775', 'Moma bubble tea bar');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('Siththi@demp.com', 'banying', '1234567890', 'นายเอกสิทธิ์', 'เวชวงศ์', '0803082507');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Seobinggo', 'TMB Bank', '7728103481', 'Seobinggo');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('inthanonphupha@act.com', 'texaschicken', '1234567890', 'นายอินทนนท์', 'ดอยภูผา', '0807072492');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Texas Chicken', 'Bangkok Bank', '3754666665', 'Texas Chicken');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('prayhad.chan-so-pha@ncpo.com', 'depoo', '1234567890', 'นายประหยัด', 'จันทร์โสภา', '0821032497');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Naeki Sushi', 'TMB Bank', '2673713617', 'Naeki Sushi');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม'), (LAST_INSERT_ID(), 'สวนหลวง');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('cheidchu.w@gmail.com', 'sefah', '1234567890', 'นายเชิดชู', 'วิทยาการ', '0829082504');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Yayoi', 'Krungsri', '7131405437', 'Yayoi');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('bankrungreung@ffp.com', 'rodsaniyom', '1234567890', 'นายธนาคาร', 'จึงเจริญรุ่งเรือง', '0825112521');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'สีฟ้า', 'Krungsri', '7901409154', 'นายธนาคาร จึงเจริญรุ่งเรือง');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('sudaketrath@ptp.com', 'jeffersteak', '1234567890', 'นางสุดาพรรณ', 'เกตุรัตน์', '0801052504');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Burger King', 'Siam Commercial Bank', '4749295342', 'นางสุดาพรรณ เกตุรัตน์');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม'), (LAST_INSERT_ID(), 'สามย่าน');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('utsaha@pprp.com', 'foodplus', '1234567890', 'นายอุตสาห', 'เสริมยานยนต์', '0819052503');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'ร้านรสนิยม', 'Krungthai Bank', '3165887889', 'นายอุตสาห เสริมยานยนต์');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('chatturas.sophon@act.com', 'dairyqueen', '1234567890', 'นายจัตุรัส', 'โสภณการ', '0805102508');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'เดอ ตลาดพลู', 'Siam Commercial Bank', '2844355175', 'เดอ ตลาดพลู');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('bunmet@ptp.com', 'mcdonald', '1234567890', 'นายบุญมี', 'เตยาพรหม', '0808072503');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'เจ๊อ้วน foodplus', 'Krungsri', '8016832152', 'เจ๊อ้วน foodplus');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('realdonald@rep.us', 'seobinggo', '1234567890', 'นายโดนัล', 'เจริญธรรม', '0814062489');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Pablo', 'Krungthai Bank', '1381922707', 'นายโดนัล เจริญธรรม');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('terazel@con.com', 'kfcnotmc', '1234567890', 'นางเทราเซล', 'มาร์', '0801102499');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'MOS Burger', 'TMB Bank', '2160232959', 'MOS Burger');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('srichongphing@com.com', 'khunnak', '1234567890', 'นายศรี', 'จงผัง', '0815062496');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'KFC', 'Siam Commercial Bank', '7586989600', 'KFC');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('him@com.kp', 'kinny', '1234567890', 'นายฮิม', 'คย็อง', '0808012527');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'อินเตอร์', 'Siam Commercial Bank', '1402689857', 'นายฮิม คย็อง');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('zenhun@hotmail.com', 'mamahouse', '1234567890', 'นายอัครมหาเซน', 'ฮุนโชเด', '0804042494');
INSERT INTO `shop`(`shop_id`, `shop_name`, `bank_name`, `bank_account`, `account_name`) VALUES
(LAST_INSERT_ID(), 'Coco Ichibanya', 'TMB Bank', '1030417500', 'นายอัครมหาเซน ฮุนโชเด');
INSERT INTO `shop_location`(`shop_id`, `location`) VALUES
(LAST_INSERT_ID(), 'สยาม');







INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('fist@customer.com', 'firstCus', '1234567890', 'first', 'customer', '0826072492');
INSERT INTO `customer`(`cus_id`) VALUES (LAST_INSERT_ID());

INSERT INTO `user`(`email`, `username`, `password`, `fname`, `lname`, `telephone`) VALUES
('second@customer.com', 'SecondCus', '1234567890', 'second', 'customer', '0826072828');
INSERT INTO `customer`(`cus_id`) VALUES (LAST_INSERT_ID());





INSERT INTO `admin`(`username`, `password`) VALUES ('admin', 'password');




INSERT INTO `gasha`(`location`, `price`, `type`) VALUES 
('สยาม','10','อาหารญี่ปุ่น'),
('จามจุรีสแควร์','10','อาหารญี่ปุ่น'),
('สวนหลวง','10','อาหารญี่ปุ่น'),
('สามย่าน','10','อาหารญี่ปุ่น'),
('สยาม','19','อาหารญี่ปุ่น'),
('จามจุรีสแควร์','19','อาหารญี่ปุ่น'),
('สวนหลวง','19','อาหารญี่ปุ่น'),
('สามย่าน','19','อาหารญี่ปุ่น'),
('สยาม','28','อาหารญี่ปุ่น'),
('จามจุรีสแควร์','28','อาหารญี่ปุ่น'),
('สวนหลวง','28','อาหารญี่ปุ่น'),
('สามย่าน','28','อาหารญี่ปุ่น'),
('สยาม','37','อาหารญี่ปุ่น'),
('จามจุรีสแควร์','37','อาหารญี่ปุ่น'),
('สวนหลวง','37','อาหารญี่ปุ่น'),
('สามย่าน','37','อาหารญี่ปุ่น'),
('สยาม','10','ของหวาน+เครื่องดื่ม'),
('จามจุรีสแควร์','10','ของหวาน+เครื่องดื่ม'),
('สวนหลวง','10','ของหวาน+เครื่องดื่ม'),
('สามย่าน','10','ของหวาน+เครื่องดื่ม'),
('สยาม','19','ของหวาน+เครื่องดื่ม'),
('จามจุรีสแควร์','19','ของหวาน+เครื่องดื่ม'),
('สวนหลวง','19','ของหวาน+เครื่องดื่ม'),
('สามย่าน','19','ของหวาน+เครื่องดื่ม'),
('สยาม','28','ของหวาน+เครื่องดื่ม'),
('จามจุรีสแควร์','28','ของหวาน+เครื่องดื่ม'),
('สวนหลวง','28','ของหวาน+เครื่องดื่ม'),
('สามย่าน','28','ของหวาน+เครื่องดื่ม'),
('สยาม','37','ของหวาน+เครื่องดื่ม'),
('จามจุรีสแควร์','37','ของหวาน+เครื่องดื่ม'),
('สวนหลวง','37','ของหวาน+เครื่องดื่ม'),
('สามย่าน','37','ของหวาน+เครื่องดื่ม'),
('สยาม','10','อาหารไทย'),
('จามจุรีสแควร์','10','อาหารไทย'),
('สวนหลวง','10','อาหารไทย'),
('สามย่าน','10','อาหารไทย'),
('สยาม','19','อาหารไทย'),
('จามจุรีสแควร์','19','อาหารไทย'),
('สวนหลวง','19','อาหารไทย'),
('สามย่าน','19','อาหารไทย'),
('สยาม','28','อาหารไทย'),
('จามจุรีสแควร์','28','อาหารไทย'),
('สวนหลวง','28','อาหารไทย'),
('สามย่าน','28','อาหารไทย'),
('สยาม','37','อาหารไทย'),
('จามจุรีสแควร์','37','อาหารไทย'),
('สวนหลวง','37','อาหารไทย'),
('สามย่าน','37','อาหารไทย'),
('สยาม','10','ฟาสต์ฟู้ด'),
('จามจุรีสแควร์','10','ฟาสต์ฟู้ด'),
('สวนหลวง','10','ฟาสต์ฟู้ด'),
('สามย่าน','10','ฟาสต์ฟู้ด'),
('สยาม','19','ฟาสต์ฟู้ด'),
('จามจุรีสแควร์','19','ฟาสต์ฟู้ด'),
('สวนหลวง','19','ฟาสต์ฟู้ด'),
('สามย่าน','19','ฟาสต์ฟู้ด'),
('สยาม','28','ฟาสต์ฟู้ด'),
('จามจุรีสแควร์','28','ฟาสต์ฟู้ด'),
('สวนหลวง','28','ฟาสต์ฟู้ด'),
('สามย่าน','28','ฟาสต์ฟู้ด'),
('สยาม','37','ฟาสต์ฟู้ด'),
('จามจุรีสแควร์','37','ฟาสต์ฟู้ด'),
('สวนหลวง','37','ฟาสต์ฟู้ด'),
('สามย่าน','37','ฟาสต์ฟู้ด');



INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 200, 0, 0, 1, 'บีแอลทีเบอร์เกอร์ (เนื้อ/หมู)', 'https://mos-th.com/common/img/menu/upload/a387580f6226c1d73cfee4ff8a0bc0e1.jpg', 'ฟาสต์ฟู้ด', 99.0, 85.0, '2019-04-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม'), (LAST_INSERT_ID(), 'จามจุรีสแควร์'), (LAST_INSERT_ID(), 'สวนหลวง');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 150, 0, 0, 1, 'ฮอท กริลด์ ชิกเก้น เบอร์เกอร์', 'https://mos-th.com/common/img/menu/upload/dfb2f2764d72863578cb74e136dfd80d.jpg', 'ฟาสต์ฟู้ด', 93.0, 85.0, '2019-05-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 190, 0, 0, 2, 'บับเบิล มิลค์ ที', 'https://static.bkkmenu.com/files/2018/06/seoulcialclub-5-670x1006.jpg', 'ของหวาน+เครื่องดื่ม', 190.0, 180.0, '2019-12-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 125, 0, 0, 2, 'ซีเรียล มิลค์ ที', 'https://static.bkkmenu.com/files/2018/06/seoulcialclub-28-670x1006.jpg', 'ของหวาน+เครื่องดื่ม', 190.0, 180.0, '2019-06-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 145, 0, 0, 3, 'Maxicana Wrap', 'http://www.projamm.com/uploads/promotion/20170529-103500.jpg', 'ฟาสต์ฟู้ด', 99.0, 70.0, '2019-11-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม'), (LAST_INSERT_ID(), 'จามจุรีสแควร์');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 145, 0, 0, 4, 'ข้าวมันไก่ตอน+ไก่ทอด 2อย่าง', 'https://img.wongnai.com/p/1968x0/2016/06/11/5460c1a7be3b4cd08356789ef9af3aa3.jpg', 'อาหารจานเดี่ยว', 80.0, 50.0, '2019-06-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 180, 0, 0, 5, 'ข้าวคลุกกะปิ', 'https://img.wongnai.com/p/1968x0/2017/09/20/e80dedbb698048c6aecf5def47ffb81e.jpg', 'อาหารไทย', 69.0, 60.0, '2019-12-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 155, 0, 0, 5, 'ไข่ครก', 'https://img.wongnai.com/p/1968x0/2016/12/14/b4994eb9e13f465997cd287f4fe4de90.jpg', 'อาหารไทย', 50.0, 45.0, '2019-03-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 195, 0, 0, 5, 'ชุด ต้นตำรับเพลินวาน', 'https://drive.google.com/file/d/1GqiMjgJjYFSrINdnIUdTu0W_QzSWcAdg/view?usp=sharing', 'อาหารไทย', 240.0, 220.0, '2019-03-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 170, 0, 0, 6, 'ปลาหมึกผัดไข่เค็ม', 'http://www.letseatthailand.com/wp-content/uploads/2015/07/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99%E0%B8%AB%E0%B8%8D%E0%B8%B4%E0%B8%87-009.jpg', 'อาหารไทย', 180.0, 150.0, '2019-08-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 190, 0, 0, 7, 'ข้าว+กับข้าว1อย่าง+ไข่ยัดใส้', 'https://img.wongnai.com/p/1968x0/2017/12/03/cb917e0e79a5487aa7b603250734cd71.jpg', 'อาหารจานเดี่ยว', 70.0, 50.0, '2019-06-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 140, 0, 0, 8, 'ซูชิเซ็ต A1', 'http://www.naekisushi.com/wp-content/uploads/2017/12/a1-768x471.jpg', 'อาหารญี่ปุ่น', 200.0, 190.0, '2019-11-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 125, 0, 0, 8, 'ซูชิเซ็ต C1', 'http://www.naekisushi.com/wp-content/uploads/2017/12/c1-768x471.jpg', 'อาหารญี่ปุ่น', 450.0, 420.0, '2019-06-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 155, 0, 0, 9, 'ชุดไก่ใหญ่เผ็ด 3', 'https://img-mcd-th.mtel.ws/images/product/1540286829129.png', 'ฟาสต์ฟู้ด', 129.0, 100.0, '2019-03-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 85, 0, 0, 9, 'แมคฟิช', 'https://img-mcd-th.mtel.ws/images/product/1540286445fish.png', 'ฟาสต์ฟู้ด', 79.0, 50.0, '2019-12-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 185, 0, 0, 9, 'Happy Meal', 'https://img-mcd-th.mtel.ws/images/product/1540354243hm-ng.png', 'ฟาสต์ฟู้ด', 95.0, 80.0, '2019-05-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 150, 0, 0, 10, 'อุด้งหมู', 'https://images.deliveryhero.io/image/fd-th/Products/728548.jpg', 'อาหารญี่ปุ่น', 125.0, 100.0, '2019-10-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 95, 0, 0, 10, 'ซาชิมิแซลมอน', 'https://images.deliveryhero.io/image/fd-th/Products/728535.jpg', 'อาหารญี่ปุ่น', 125.0, 100.0, '2019-07-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 155, 0, 0, 11, 'ชุดสเต๊กปลาแซลมอน', 'http://www.fuji.co.th/2009/TH/fujimenu/pic-menu/055-1.jpg', 'อาหารญี่ปุ่น', 320.0, 290.0, '2019-09-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 90, 0, 0, 11, 'ชุดปลาซาบะย่างซีอิ๊วหรือย่างเกลือ', 'http://www.fuji.co.th/2009/TH/fujimenu/pic-menu/081.jpg', 'อาหารญี่ปุ่น', 160.0, 150.0, '2019-05-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 195, 0, 0, 12, 'ข้าวแกงกะหรีหมูชุบเกล็ดขนมปังทอด', 'http://www.cocothailand.com/images/menu/img-menu-100.png', 'อาหารญี่ปุ่น', 160.0, 150.0, '2019-03-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 150, 0, 0, 12, 'ซุปข้าวโพด', 'http://www.cocothailand.com/images/menu/img-menu-600.png', 'อาหารญี่ปุ่น', 70.0, 50.0, '2019-08-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 80, 0, 0, 13, 'ข้าวยำไก่แซ่บ', 'http://3.bp.blogspot.com/_9aOUxhyu9rc/SJmpM81UqYI/AAAAAAAAABU/rcmy0uttZD4/s400/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%A2%E0%B8%B3%E0%B9%84%E0%B8%81%E0%B9%88%E0%B9%81%E0%B8%8B%E0%B9%88%E0%B8%9A.jpg', 'ฟาสต์ฟู้ด', 59.0, 50.0, '2019-10-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 110, 0, 0, 13, 'ข้าวยำไก่ซี้ด', 'https://nunameza1991.files.wordpress.com/2013/02/6.jpg', 'ฟาสต์ฟู้ด', 59.0, 50.0, '2019-09-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 160, 0, 0, 14, 'บลิซซาร์ด ขนาด XL', 'https://www.punpromotion.com/wp-content/uploads/2018/11/45527268_2036028413100168_4942119576453251072_n.jpg', 'ของหวาน+เครื่องดื่ม', 80.0, 50.0, '2019-11-30');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 175, 0, 0, 15, 'ชุุดสุดคุ้ม บาร์บีคิวเบคอน ชีส', 'https://buzzebees.blob.core.windows.net/campaigns/252010-large?time=20181127023502', 'ฟาสต์ฟู้ด', 185.0, 150.0, '2019-03-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');

INSERT INTO `coupon_req`(`status`, `total_amount`, `sold_out_amount`, `used_amount`, `shop_id`, `food_name`, `food_image`, `food_type`, `actual_price`, `offer_price`, `expiration`) VALUES
('waiting', 125, 0, 0, 15, 'เอ็กซ์ตร้าลอง ชีสเบอร์เกอร์', 'https://buzzebees.blob.core.windows.net/campaigns/251967-large?time=20181127023127', 'ฟาสต์ฟู้ด', 179.0, 150.0, '2019-05-31');
INSERT INTO `coupon_location`(`req_id`, `food_location`) VALUES
( LAST_INSERT_ID(), 'สยาม');








-- INSERT INTO `include_coupon`(`gasha_id`, `req_id`, `rarity`, `rate`) VALUES
-- (31, 1, 'R', 25.00),
-- (34, 1, 'C', 55.00),
-- (36, 1, 'C', 55.00),
-- (31, 2, 'C', 55.00),
-- (34, 2, 'C', 55.00),
-- (36, 2, 'C', 55.00),
-- (8, 3, 'SSR', 5.00),
-- (12, 3, 'SSR', 5.00),
-- (16, 3, 'SR', 15.00),
-- (18, 3, 'R', 25.00),
-- (8, 4, 'SSR', 5.00),
-- (12, 4, 'SSR', 5.00),
-- (16, 4, 'SR', 15.00),
-- (18, 4, 'R', 25.00),
-- (31, 5, 'R', 25.00),
-- (34, 5, 'C', 55.00),
-- (36, 5, 'C', 55.00),
-- (22, 7, 'C', 55.00),
-- (25, 7, 'C', 55.00),
-- (28, 7, 'C', 55.00),
-- (22, 8, 'C', 55.00),
-- (25, 8, 'C', 55.00),
-- (28, 8, 'C', 55.00),
-- (22, 9, 'SSR', 5.00),
-- (25, 9, 'SSR', 5.00),
-- (28, 9, 'SR', 15.00),
-- (22, 10, 'SSR', 5.00),
-- (25, 10, 'SR', 15.00),
-- (28, 10, 'C', 55.00),
-- (4, 12, 'SR', 15.00),
-- (6, 12, 'R', 25.00),
-- (4, 13, 'SSR', 5.00),
-- (6, 13, 'SSR', 5.00),
-- (31, 14, 'SR', 15.00),
-- (34, 14, 'C', 55.00),
-- (36, 14, 'C', 55.00),
-- (31, 15, 'C', 55.00),
-- (34, 15, 'C', 55.00),
-- (36, 15, 'C', 55.00),
-- (31, 16, 'R', 25.00),
-- (34, 16, 'C', 55.00),
-- (36, 16, 'C', 55.00),
-- (4, 17, 'C', 55.00),
-- (6, 17, 'C', 55.00),
-- (4, 18, 'C', 55.00),
-- (6, 18, 'C', 55.00),
-- (4, 19, 'SSR', 5.00),
-- (6, 19, 'SSR', 5.00),
-- (4, 20, 'R', 25.00),
-- (6, 20, 'C', 55.00),
-- (4, 21, 'R', 25.00),
-- (6, 21, 'C', 55.00),
-- (4, 22, 'C', 55.00),
-- (6, 22, 'C', 55.00),
-- (31, 23, 'C', 55.00),
-- (34, 23, 'C', 55.00),
-- (36, 23, 'C', 55.00),
-- (31, 24, 'C', 55.00),
-- (34, 24, 'C', 55.00),
-- (36, 24, 'C', 55.00),
-- (8, 25, 'SSR', 5.00),
-- (12, 25, 'C', 55.00),
-- (16, 25, 'C', 55.00),
-- (18, 25, 'C', 55.00),
-- (31, 26, 'SSR', 5.00),
-- (34, 26, 'SR', 15.00),
-- (36, 26, 'R', 25.00),
-- (31, 27, 'SSR', 5.00),
-- (34, 27, 'SR', 15.00),
-- (36, 27, 'C', 55.00);
